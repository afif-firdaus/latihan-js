// Latihan 1

/*
Create a function which returns the number of true values there are in an array.
*/

function hitungBenar(arr) {
  let hitung = arr.filter(Boolean).length;
  return hitung;
}

console.log(hitungBenar([true, false, false, true, false]));
//  ➞ 2

console.log(hitungBenar([false, false, false, false]));
//  ➞ 0

console.log (hitungBenar([]));
// ➞ 0

console.log(`\n\n`);

// Latihan 2
/*
Play with asterisk (bintang)
*/
// Test 1
/*
Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript untuk menampilkan di console barisan asterisks (bintang). Setiap baris memiliki 1 simbol '*'.
*/

var rows1 = 5; // input the number of rows
// do loops to display asterisks in the console.

  for (let i = 1; i <= rows1; i++) {
    console.log('*');
  }

  console.log(`\n\n`);

/* 
Expect output

jika rows1 = 5

<______________________________>

*
*
*
*
*




*/


// Test 2

/*
Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript untuk menampilkan di console barisan asterisks (bintang). Setiap baris memiliki jumlah simbol '*' sesuai dengan jumlah baris. Manfaatkan nested loop atau loop di dalam loop untuk menyelesaikan kasus ini.
*/

var rows2 = 5; // input the number of rows
var bintang1 = '';
// do loops to display asterisks in the console.
for (let j = 0; j < rows2; j++) {
  for (let k = 0; k < rows2; k++) {
    bintang1 = bintang1 + '*';
  }
  console.log(bintang1); 
  bintang1 = '';
}

console.log(`\n\n`);
/*

jika rows2 = 5

<______________________________>

*****
*****
*****
*****
*****


*/


// Test 3

/*

Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript untuk menampilkan di console barisan asterisks (bintang) dalam bentuk tangga. Setiap baris memiliki jumlah simbol '*' sesuai dengan nomor baris. Baris pertama, hanya ada satu bintang. Baris kedua, dua bintang, baris ketiga tiga bintang, dan seterusnya. Manfaatkan nested loop atau loop di dalam loop untuk menyelesaikan kasus ini.


*/


var rows3 = 5; // input the number of rows
var bintang2 = '';
// do loops to display asterisks in the console.

for (let l = 1; l <= rows3; l++) {
  for (let m = 1; m <= l; m++) {
    bintang2 = bintang2 + '*'; 
  }
  console.log(bintang2);
  bintang2 = '';
}

console.log(`\n`);
// /*

// jika rows3 = 5

// <______________________________>

// *
// **
// ***
// ****
// *****


// */